# Assessment 1's README

# How to commit my codes #
* git add .
* git commit -m <message>
* git push original master -u


# SETUP #
This is a ASSESSMENT DAY setup steps
* git init
* git remote add <gitUrl>
* npm init (ans all question, enter entry point accordingly)
* mkdir server directory
* create app.js under server dir
* create .gitigonore to ignore directories/files

# USAGE #
* Just run localhost:3000
* Fill in the form (make sure all fields are filled)
* Click Submit
* You should received a reply from server with time stamp.