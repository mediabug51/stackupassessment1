/**
 * Client side code.
 */
(function () {
    "use strict";
    angular.module("myApp").controller("RegisterForm", RegisterForm);
    
    RegisterForm.$inject = ["$http"];

    function RegisterForm($http) {
        var self = this; // vm

        //init setup
        self.user = {};
        self.initForm = initForm;
        self.onSubmit = onSubmit;
        self.onReset = onReset;
        self.verifyAge = verifyAge;

        self.countries = [
            { name: "Please select" , value:""},
            { name: "Singapore", value: "Singapore"},
            { name: "Indonesia", value: "Indonesia"},
            { name: "Thailand", value: "Thailand"},
            { name: "Malaysia", value: "Malaysia"},
            { name: "Australia", value: "Australia"},  
            { name: "Others ...", value: "Others"}   
        ];
        
        self.validationPattern = {
            email : /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/i,
            password : /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
            phone : /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/
       };

        //functions defination
        function initForm() {
            self.user.nationalities = 0
            self.isSubmitted = false;
        }

        function onSubmit() {
            console.log('Your have submitted PW: ' + self.user.password);
            self.isSubmitted = true;

            $http.post("/api/register", self.user).then(function (returnData) {
                console.log('Returned from Server: ' + JSON.stringify(returnData));
                self.user.returnMessage = returnData.data.message;
            }).catch(function (data) {
                console.log('ERROR from Server: ' + JSON.stringify(returnData));
            });;
        }

        function onReset() {
           // self.user = {};
            Object.assign({}, self.user);
            self.registrationform.$setPristine();
            self.registrationform.$setUntouched();
        }

        function verifyAge() {
            var validAge=true;

            if (self.user.dob!=undefined) {
                self.user.age = calcAge(self.user.dob);
                var validAge = (self.user.age>=18);
                if (validAge==false) {
                    //self.registrationform.$invalid = true;
                    //console.log('Underage');
                }
            }
          
            return validAge;
        }

        //internal functions
        function calcAge(theDate) {
            var birthday = new Date(theDate);
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        //triggers initialization
        self.initForm();
    }

})();